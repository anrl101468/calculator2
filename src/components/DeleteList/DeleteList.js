import { observer } from "mobx-react";
import { useStores } from "../../state/context/Context";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

export const DeleteList = observer((props) => {
	const { eventStore } = useStores();

	console.log(props.event);

	const handleDeleteList = () => {
		if (window.confirm("삭제하시겠습니까?")) {
			eventStore.removeEvent(props.event.id);
		}
	};

	return (
		<IconButton onClick={handleDeleteList}>
			<DeleteIcon />
		</IconButton>
	);
});
