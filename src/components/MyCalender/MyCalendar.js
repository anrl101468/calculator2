import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Toolbar, { MyToolbar } from "../Toolbar/MyToolbar";
import { useCallback, useState } from "react";
import { observer } from "mobx-react";
import { useStores } from "../../state/context/Context";
import { EventStore } from "../../state/store/EventStore/EventStore";

// const myEventsList = [
// 	{
// 		start: new Date(),
// 		end: new Date(),
// 		// end: new Date(moment().add(1, "day")),
// 		title: props.title,
// 	},
// ];

const localizer = momentLocalizer(moment);

const DnDCalendar = withDragAndDrop(Calendar);

export const MyCalendar = observer((props) => {
	const { eventStore, calendarStore } = useStores();

	return (
		<DnDCalendar
			localizer={localizer}
			defaultView="month"
			views={["month"]}
			events={eventStore.eventList}
			startAccessor="start"
			endAccessor="end"
			onSelectSlot={props.handleSelectSlot}
			style={{ height: calendarStore.calendar.height, width: calendarStore.calendar.width }}
			// onSelectEvent={onSelectEvent}
			// selectable={onSelectEvent}
			eventPropGetter={props.eventPropGetter}
			onEventDrop={props.handleOnEventDrop}
			draggableAccessor={(event) => true}
			key={calendarStore.currentDate}
			selectable={calendarStore.calendar.selectable}
			dayPropGetter={props.handleDayPropGetter}
			components={{ toolbar: MyToolbar }}
			popup
		/>
	);
});
