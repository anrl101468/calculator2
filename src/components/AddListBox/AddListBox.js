import { Box, Button, TextField } from "@mui/material";
import { observer } from "mobx-react";
import { useEffect } from "react";
import { useStores } from "../../state/context/Context";
import CloseIcon from "@mui/icons-material/Close";
import { toJS } from "mobx";
import moment from "moment";
import { TypeSelect } from "../TypeSelect/TypeSelect";
import { DeleteList } from "../DeleteList/DeleteList";

export const AddListBox = observer(() => {
	const { eventStore, calendarStore } = useStores();

	const handleTextBoxOnChange = (value, id) => {
		const eventIndex = eventStore.eventList.findIndex((e) => e.id === id);
		eventStore.setEvent("title", value, eventIndex);
		localStorage.setItem("eventList", JSON.stringify(toJS(eventStore.eventList)));
	};

	const handleMyScheduleListEnterPress = (e) => {
		// console.log(e.target.value);
		if (e.key === "Enter" && e.target.value !== "") {
			const day = moment(calendarStore.currentDate).format("YYYY-MM-DD");
			const event = calendarStore.createEvent(e.target.value, day, day);
			eventStore.pushEventList(event);
			localStorage.setItem("eventList", JSON.stringify(toJS(eventStore.eventList)));
			e.target.value = "";
		}
	};

	return (
		<>
			{eventStore.eventList
				.filter((e) => e.start == calendarStore.currentDate)
				.map((e, i) => {
					return (
						<Box style={{ display: "flex", alignItems: "center", gap: "5px" }} key={calendarStore.currentDate + i}>
							<TypeSelect event={e} />
							<TextField
								id="standard-basic"
								label="할일"
								variant="standard"
								value={e.title}
								onChange={(event) => {
									handleTextBoxOnChange(event.target.value, e.id);
								}}
							></TextField>
							<DeleteList event={e} />
						</Box>
					);
				})}

			<TextField
				id="standard-basic"
				label="할일"
				variant="standard"
				onKeyDown={handleMyScheduleListEnterPress}
				placeholder="할일을 적어주세요"
			/>
		</>
	);
});
