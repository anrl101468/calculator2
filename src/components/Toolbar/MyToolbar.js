import React, { useEffect, useState } from "react";
import { Navigate } from "react-big-calendar";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import moment from "moment";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import { observer } from "mobx-react-lite";
import { useStores } from "../../state/context/Context";

export const MyToolbar = observer(({ label, onNavigate }) => {
	const { eventStore, calendarStore } = useStores();
	const [currentMonth, setCurrentMonth] = useState("");
	const date = moment(label, "MMMM YYYY");

	useEffect(() => {
		setCurrentMonth(moment(date).format("YYYY-MM"));
	}, []);

	const goToPrevMonth = () => {
		onNavigate(Navigate.PREVIOUS);
		setCurrentMonth(moment(date).add(-1, "month").format("YYYY-MM"));
	};
	const goToNextMonth = () => {
		onNavigate(Navigate.NEXT);
		setCurrentMonth(moment(date).add(1, "month").format("YYYY-MM"));
	};

	const customLabel = date.format("YYYY년 MM월");

	return (
		// <div className="rbc-toolbar">
		// 	<span className="rbc-btn-group">
		// 		<button type="button" onClick={navigate.bind(null, "TODAY")}>
		// 			Today
		// 		</button>
		// 		<button type="button" onClick={navigate.bind(null, "PREV")}>
		// 			이전
		// 		</button>
		// 		<span className="rbc-toolbar-label">{`${date.getFullYear()}년 ${date.getMonth() + 1}월`}</span>
		// 		<button type="button" onClick={navigate.bind(null, "NEXT")}>
		// 			다음
		// 		</button>
		// 	</span>
		// </div>
		<div style={{ display: "flex", justifyContent: "space-around" }}>
			<ArrowBackIcon onClick={goToPrevMonth}>Prev</ArrowBackIcon>
			<div style={{ display: "flex" }}>
				{customLabel}
				<div style={{ display: "flex", marginLeft: "20px" }}>
					<div style={{ display: "flex", marginLeft: "10px" }}>
						<CheckBoxOutlineBlankIcon />
						<span>{eventStore.getMatchedEventTypeCount("default", currentMonth)}</span>
					</div>
					<div style={{ display: "flex", marginLeft: "10px" }}>
						<CheckBoxIcon />
						<span>{eventStore.getMatchedEventTypeCount("checked", currentMonth)}</span>
					</div>
				</div>
			</div>
			<ArrowForwardIcon onClick={goToNextMonth}>Next</ArrowForwardIcon>
		</div>
	);
});
