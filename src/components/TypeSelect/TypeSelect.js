import { observer } from "mobx-react";
import { useStores } from "../../state/context/Context";
import { useEffect, useState } from "react";
import { Checkbox } from "@mui/material";

export const TypeSelect = observer((props) => {
	const { eventStore } = useStores();

	console.log(props.event.type);

	//false로 시작
	const [checked, setChecked] = useState(props.event.type === "checked");

	useEffect(() => {
		setChecked(props.event.type === "checked");
	}, [props.event.type]);

	const handleChange = (event) => {
		const isChecked = event.target.checked;
		setChecked(isChecked);
		handleEventTypeOnChange(isChecked ? "checked" : "default", props.event.id);
	};

	const handleEventTypeOnChange = (value, id) => {
		const eventIndex = eventStore.eventList.findIndex((e) => e.id === id);
		eventStore.setEvent("type", value, eventIndex);
	};

	return <Checkbox checked={checked} onChange={handleChange} value={checked ? "checked" : "default"} />;
});
