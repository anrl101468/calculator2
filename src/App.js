import { observer } from "mobx-react";
import "./App.css";
import ToDoList from "./page/CalenderPage";

const App = observer(() => {
	return (
		<>
			<ToDoList />
		</>
	);
});

export default App;
