import { observable } from "mobx";
import { CalendarStore } from "./CalenderStore/CalenderStore";
import { EventStore } from "./EventStore/EventStore";

export const RootStore = () => {
	const store = observable({
		calendarStore: CalendarStore(),
		eventStore: EventStore(),
	});

	return store;
};
