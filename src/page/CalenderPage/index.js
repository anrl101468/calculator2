// import { useEffect, useState } from "react";
import { useStores } from "../../state/context/Context";
import { observer } from "mobx-react";

import { MyCalendar } from "../../components/MyCalender/MyCalendar";
import moment from "moment";
import { AddListBox } from "../../components/AddListBox/AddListBox";
import { useCallback, useEffect } from "react";
import { Box } from "@mui/material";
import { Padding } from "@mui/icons-material";

export const ToDoList = observer((props) => {
	const { eventStore, calendarStore } = useStores();
	const height = window.innerHeight - 300;

	console.log(props.event);

	useEffect(() => {
		calendarStore.initCalendar(height, window.innerWidth - 800, true);
		eventStore.initEventList(JSON.parse(localStorage.getItem("eventList")));
	}, []);

	const handleMyCalendarSelectSlot = ({ start }) => {
		calendarStore.setCurrentDate(moment(start).format("YYYY-MM-DD"));
		localStorage.setItem("currentDate", moment(start).format("YYYY-MM-DD"));
		localStorage.setItem("currentMonth", moment(start).format("YYYY-MM"));
	};

	const handleMyCalendarDayPropGetter = (date) => {
		if (moment(date).isSame(calendarStore.currentDate, "day")) {
			return {
				style: {
					backgroundColor: "#A6C3E5",
					cursor: "pointer",
				},
			};
		}
		return {
			style: {
				cursor: "pointer",
			},
		};
	};

	const eventPropGetter = useCallback(
		(event) => ({
			...(event.type == "default" && {
				style: {
					backgroundColor: "#216BBF",
				},
			}),
			...(event.type == "checked" && {
				style: {
					backgroundColor: "darkBlue",
				},
			}),
		}),
		[]
	);

	return (
		<>
			<div style={{ display: "flex", gap: "20px" }}>
				<MyCalendar
					handleSelectSlot={handleMyCalendarSelectSlot}
					calendar={calendarStore.calendar}
					events={eventStore.eventList}
					handleDayPropGetter={handleMyCalendarDayPropGetter}
					eventPropGetter={eventPropGetter}
					handleOnEventDrop={eventStore.moveEvent}
				/>
				<Box style={{ paddingTop: "30px", display: "flex", flexDirection: "column", gap: "20px" }}>
					<AddListBox />
				</Box>
			</div>
		</>
	);
});

export default ToDoList;
